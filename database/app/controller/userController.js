//import thư viện mongoose
const mongoose = require("mongoose");

//import Review Model
const userModel = require("../models/userModel")

const getAllUser  = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    userModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all user: Successfull",
            data: data
        })
    })
}
const createUser = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "bodyFullName" : "Le Quang Tuan",
    //     "bodyEmail" : "maitostaham@gmail.com",
    //     "bodyAddress": "Sài Gòn",
    //     "bodyPhone" : "0965127997"
    // }

    // Bước 2: validate dữ liệu
    if(!body.bodyFullName){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "FullName không hợp lệ" 
        })
    }
    if(!body.bodyEmail){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Email không hợp lệ" 
        })
    }
    if(!body.bodyAddress){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Address không hợp lệ" 
        })
    }
    if(!body.bodyPhone){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Phone không hợp lệ" 
        })
    }
    // bước 3: Gọi Model tạo dữ liệu
    const newUser = {
        fullName: body.bodyFullName,
        email: body.bodyEmail,
        address: body.bodyAddress,
        phone: body.bodyPhone
    };
    //bước 4: trả về kết quả 
    userModel.create(newUser, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status: "Create User: Successfull",
            data: data
        })
    } );
}
const getUserByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const userID =  request.params.userId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userID)){
     return response.status(400).json({
             status: "Bad request",
             message: "UserId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
     userModel.findById(userID,(error,data) =>{
         return response.status(200).json({
             status: "Get Detail User successfull",
             data: data
             })
         }
     )  
 }
 const updateUserByID = (request,response) => {
    // bước 1: chuẩn bị dữ liệu
    const userId =  request.params.userId;
    const body = request.body;

   //bước 2: Validate dữ liệu
   if(!mongoose.Types.ObjectId.isValid(userId)){
    return response.status(400).json({
        status: "Bad request",
        message: "userId không hợp lệ" 
    })
    }
    if(body.bodyFullName !== undefined && body.bodyFullName.trim() === ""){
        return response.status(400).json({
            status: "Bad request",
            message: "FullName không hợp lệ"
        })
    }
    if(body.bodyEmail !== undefined && body.bodyEmail.trim() === ""){
        return response.status(400).json({
            status: "Bad request",
            message: "Email không hợp lệ"
        })
    }
    if(body.bodyAddress !== undefined && body.bodyAddress.trim() === ""){
        return response.status(400).json({
            status: "Bad request",
            message: "Address không hợp lệ"
        })
    }
    if(body.Phone !== undefined && body.bodyPhone.trim() === ""){
        return response.status(400).json({
            status: "Bad request",
            message: "Phone không hợp lệ"
        })
    }
    //bước 3: Gọi Model tạo dữ liệu
    const updateUser = {}
    updateUser.fullName = body.bodyFullName;
    updateUser.email = body.bodyEmail;
    updateUser.address = body.bodyAddress;
    updateUser.phone = body.bodyPhone;
    //bước 4: trả về kết quả 
    userModel.findByIdAndUpdate(userId, updateUser,{new:true}, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update user successfully", 
            data: data
        })
    })
}
const deleteUserById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const userId = request.params.userId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "UserId không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    userModel.findByIdAndDelete(userId, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete user successfully"
        })
    })
}
module.exports = {
    getAllUser,
    createUser,
    getUserByID,
    updateUserByID,
    deleteUserById
}