//Import thư viện Mongoose
const { response, request, json } = require("express");
const  mongoose =  require("mongoose");

//import Modle Drink vào file
const voucherModel = require("../models/voucherModel");

const createVoucher = (request, response) => {
    //bước 1: chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "bodyMaVoucher" : "123123",
    //     "bodyPhanTramGiamGia" : 15,
    //     "bodyGhiChu" : "Hello"
    // }
    //bước 2: validate dữ liệu
    //kiểm tra maVoucher có hợp lệ hay không 
    if(!body.bodyMaVoucher){
        return response.status(400).json({
            status: "Bad Request",
            message: "maVoucher không hợp lệ",
        })
    }
    //kiểm tra phanTramGiamGia 
    if(isNaN(body.bodyPhanTramGiamGia) && body.bodyPhanTramGiamGia < 0 && !body.bodyPhanTramGiamGia){
        return response.status(400).json({
            status: "Bad Request",
            message: "maGiamGia không hợp lệ",
        })
    }
    //kiểm tra ghiChu có hợp lệ hay không
    if(!body.bodyGhiChu){
        return response.status(400).json({
            status: "Bad Request",
            message: "Ghi chú không hợp lệ"
        })
    }
    //bước 3: Gọi Model tạo dữ liệu
    const newVoucher = {
        maVoucher: body.bodyMaVoucher,
        phanTramGiamGia: body.bodyPhanTramGiamGia,
        ghiChu: body.bodyGhiChu
    }
    //bước 4: Trả về kết quả 
    voucherModel.create(newVoucher,(error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message 
            })
        }
        return response.status(201).json({
            status: "Create voucher: Successfull",
            data:  data
        })
    });
}
const getAllvoucher = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    // bước 4: Trả về kết quả
    voucherModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all voucher: Successfull",
            data: data
        })
    })
}
const getVoucherByMaVoucher = (request,response) =>{
    // bước 1: chuẩn bị dữ liệu
   const voucherId  =  request.params.voucherId;
   //bước 2: Validate dữ liệu
    //bước 3 - 4: gọi model tạo dữ liêu  và trả về 
    voucherModel.exists({maVoucher:voucherId}, (err, result) => {
        console.log("Finding Voucher...")
        if(err){
            return response.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        } 
        else{
            if(result !== null){
                voucherModel.find({maVoucher:voucherId}).exec((error,data) =>{
                        if(error){
                            return response.status(500).json({
                                status: "Internal server error",
                                message: error.message
                            })
                        }
                        return response.status(200).json({
                            status: "Get Detail Voucher successfull",
                            data: data
                            })
                        }
                    )  
            }else{
                return response.status(400).json({
                    status: "BAD REQUEST",
                    message: "Voucher not exits!"
                })
            }
        }
    })
    // voucherModel.find({maVoucher:voucherId}).exec((error,data) =>{
    //     if(error){
    //         return response.status(500).json({
    //             status: "Internal server error",
    //             message: error.message
    //         })
    //     }
    //     return response.status(200).json({
    //         status: "Get Detail Voucher successfull",
    //         data: data
    //         })
    //     }
    // )  
}
const updateVoucherById = (request, response) =>{
    //bước 1: chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;
    const body = request.body; //lấy thông tin từ json
 
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
         return response.status(400).json({
             status: "Bad request",
             message: "voucherId không hợp lệ" 
         })
    } 
    if(body.bodyMaVoucher !== undefined && body.bodyMaVoucher.trim() === ""){
         return response.status(400).json({
             status: "Bad Request",
             message: "maVoucher không hợp lệ"
         })
    }
    if(body.bodyPhanTramGiamGia !== undefined && (isNaN(body.bodyPhanTramGiamGia) || body.bodyPhanTramGiamGia < 0) ){
        return response.status(400).json({
            status: "Bad Request",
            message: "phanTramGiamGia không hợp lệ"
        })
    }
    if(body.bodyGhiChu !== undefined && body.bodyGhiChu.trim() === ""){
     return response.status(400).json({
         status: "Bad Request",
         message: "ghiChu không hợp lệ"
     })
    }
   
     //bước 3: Gọi model tạo dữ liệu
     const updateVoucher= {};
 
     if(body.bodyMaVoucher !== undefined){
        updateVoucher.maVoucher = body.bodyMaVoucher;
     }
     if(body.bodyPhanTramGiamGia !== undefined){
        updateVoucher.phanTramGiamGia = body.bodyPhanTramGiamGia;
     }
     if(body.bodyGhiChu !== undefined){
        updateVoucher.ghiChu = body.bodyPhanTramGiamGia;
     }
      //bước 4: trả về kết quả 
      voucherModel.findByIdAndUpdate(voucherId, updateVoucher,{new: true}, (error, data) => {
         if(error){
             return response.status(500).json({
                 status: "Internal server error",
                 message: error.message
             })
         }
         return response.status(200).json({
             status: "Update vocuher successfully", 
             data: data
         })
     })
 }
 const deleteVoucherById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherID = request.params.voucherId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher ID không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    voucherModel.findByIdAndDelete(voucherID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Delete voucher successfully"
        })
    })
}
module.exports = {
    createVoucher,
    getAllvoucher,
    getVoucherByMaVoucher,
    updateVoucherById,
    deleteVoucherById
}