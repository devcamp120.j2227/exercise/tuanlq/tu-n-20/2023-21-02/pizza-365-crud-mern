//Import thư viện Mongoose
const { response, request } = require("express");
const  mongoose =  require("mongoose");

//import Modle Drink vào file
const drinkModel = require("../models/drinkModel");

const createDrink = (request, response) => {
    //bước 1: chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "bodyMaNuocUong" : "COCACOLA",
    //     "bodyTenNuocUong" : "cocacola",
    //     "bodyDonGia" : 10000
    // }
    //bước 2: validate dữ liệu
    // kiểm tra maNuocUong có hợp lệ hay không
    console.log(body.bodyMaNuocUong);
    if(!body.bodyMaNuocUong){
        return response.status(400).json({
            status: "Bad Request",
            message: "maNuocUong không hợp lệ"
        })
    }
    //kiểm tra tenNuocUong có hợp lệ hay không
    if(!body.bodyTenNuocUong){
        return response.status(400).json({
            status: "Bad Request",
            message: "tenNuocUong không hợp lệ"
        })
    }
    //kiểm tra đơn giá có hợp lệ hay không
    if((isNaN(body.bodyDonGia) || body.bodyDonGia <0) && !body.bodyDonGia){
        return response.status(400).json({
            status: "Bad Request",
            message: "donGia không hợp lệ"
        })
    }
    //bước 3: Gọi Model tạo dữ liệu
    const newDrink = {
        maNuocUong: body.bodyMaNuocUong,
        tenNuocUong: body.bodyTenNuocUong,
        donGia: body.bodyDonGia
    }
    //bước 4: Trả về kết quả 
    drinkModel.create(newDrink,(error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message 
            })
        }
        return response.status(201).json({
            status: "Create Drink: Successfull",
            data:  data
        })
    });
}
const getAllDrink = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    // bước 4: Trả về kết quả
    drinkModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all drink: Successfull",
            data: data
        })
    })
}
const getDrinkById = (request,response) =>{
    // bước 1: chuẩn bị dữ liệu
   const drinkId  =  request.params.drinkId;
   console.log(drinkId);
   //bước 2: Validate dữ liệu
   if(!mongoose.Types.ObjectId.isValid(drinkId)){
    return response.status(400).json({
            status: "Bad request",
            message: "Drink không hợp lệ" 
        })
    }
    //bước 3 - 4: gọi model tạo dữ liêu  và trả về 
    drinkModel.findById(drinkId,(error,data) =>{
        return response.status(200).json({
            status: "Get Detail Drink successfull",
            data: data
            })
        }
    )  
}
const updateDrinkById = (request, response) =>{
   //bước 1: chuẩn bị dữ liệu
   const drinkId = request.params.drinkId;
   const body = request.body; //lấy thông tin từ json
   console.log(body.bodyDonGia);

   //bước 2: Validate dữ liệu
   if(!mongoose.Types.ObjectId.isValid(drinkId)){
        return response.status(400).json({
            status: "Bad request",
            message: "DrinkId không hợp lệ" 
        })
   } 
   if(body.bodyMaNuocUong !== undefined && body.bodyMaNuocUong.trim() === ""){
        return response.status(400).json({
            status: "Bad Request",
            message: "maNuocUong không hợp lệ"
        })
   }
   if(body.bodyTenNuocUong !== undefined && body.bodyTenNuocUong.trim() === ""){
    return response.status(400).json({
        status: "Bad Request",
        message: "tenNuocUong không hợp lệ"
    })
    }
    if(body.bodyDonGia !== undefined && (isNaN(body.bodyDonGia) || body.bodyDonGia < 0) ){
        return response.status(400).json({
            status: "Bad Request",
            message: "maNuocUong không hợp lệ"
        })
    }
    //bước 3: Gọi model tạo dữ liệu
    const updateDrink = {};

    if(body.bodyMaNuocUong !== undefined){
        updateDrink.maNuocUong = body.bodyMaNuocUong;
    }
    if(body.bodyTenNuocUong !== undefined){
        updateDrink.tenNuocUong = body.bodyTenNuocUong;
    }
    if(body.bodyDonGia !== undefined){
        updateDrink.donGia = body.bodyDonGia;
    }
     //bước 4: trả về kết quả 
     drinkModel.findByIdAndUpdate(drinkId, updateDrink,{new: true}, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update drink successfully", 
            data: data
        })
    })
}
const deleteDrinkById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const drinkID = request.params.drinkId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(drinkID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkId không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    drinkModel.findByIdAndDelete(drinkID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Delete drink successfully"
        })
    })
}
module.exports = {
    createDrink,
    getAllDrink,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById
}