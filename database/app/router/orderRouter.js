// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

//Import review controller 
const orderController = require("../controller/orderController");

router.post("/devcamp-pizza365/orders", orderController.createOrder);

router.get("/devcamp-pizza365/orders", orderController.getAllOrder);

router.get("/devcamp-pizza365/orders/:orderId", orderController.getOrderByID);

router.put("/devcamp-pizza365/orders/:orderId", orderController.updateOrderByID);

router.delete("/order/:orderId", orderController.deleteOrderById);

router.post("/devcamp-pizza365/orders", orderController.postOrderByUser);

module.exports = router;