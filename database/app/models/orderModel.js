//import thư viện moongose
const mongoose = require("mongoose");

//class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance order Schema từ class schema
const orderSchema  = new Schema({
    orderCode: {
        type: String,
        unique: true, 
        default: function(){
            var length = 5;
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }
    },
    kichCo: {
        type: String,
    },
    duongKinh: {
        type: String,
    },
    suon: {
        type: String,
    },
    salad: {
        type: String,
    },
    loaiPizza: {
        type: String,
    },
    idVourcher: {
        type: String,
    },
    idLoaiNuocUong: {
        type: String,
    },
    soLuongNuoc: {
        type: String,
    },
    hoTen: {
        type: String,
    },
    thanhTien: {
        type: String,
    },
    email: {
        type: String,
    },
    soDienThoai: {
        type: String,
    },
    diaChi: {
        type: String,
    },
    loiNhan: {
        type: String,
    },
    trangThai: {
        type: String,
        default: "Open"
    }
},{
    timestamps:true
}, { 
    versionKey: false 
}, { 
    versionKey: false 
}
)
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Order", orderSchema )