//import thư viện moongose
const mongoose = require("mongoose");

//class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance user Schema từ class schema
const userSchema  = new Schema({
    fullName: {
        type: String,
        require: true
    },
    email:{
        type:String,
        require: true,
        unique: true,
    },
    address:{
        type:String,
        require: true,
    },
    phone:{
        type:String,
        require: true,
        unique: true,
    },
    orders:[{
            type: mongoose.Types.ObjectId,
            ref: "Order"
        }]
    }
,{
    timestamps:true
})
//biên dịch course model từ courseSchema
module.exports = mongoose.model("User", userSchema )