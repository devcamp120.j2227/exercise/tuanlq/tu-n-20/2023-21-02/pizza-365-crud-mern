//khai báo express js
const {response, request} = require("express");
const express = require("express");
//khai báo mongoose
const mongoose = require("mongoose");
mongoose.set('strictQuery', false);
//khởi tạo path
const path = require("path");

//khởi tạo app express
const app = express();

//khai báo cổng chạy app
const port = 8000;
//set up cors
var cors = require('cors')

app.use(cors())
// Khai báo routes app
const drinkRouter = require("./app/router/drinkRouter");
const voucherRouter = require("./app/router/voucherRouter");
const userRouter = require("./app/router/userRouter");
const orderRouter = require("./app/router/orderRouter");
//khai báo model 
const drinkModel = require("./app/models/drinkModel")
const voucherModel = require("./app/models/voucherModel")
const userModel= require("./app/models/userModel");
const orderModel = require("./app/models/orderModel");
//cấu hình request đọc 
app.use(express.json());
//load ảnh trong trang web
app.use(express.static(__dirname + "/views"));
//khai báo API dạng GET "/" sẽ chạy vào file pizza 365

app.get("/",(request, response) => {
    response.sendFile(path.join(__dirname + "/views/MenuPizza365-1.9.html"))
})
app.get("/api", (request,response) => {
    response.sendFile(path.join(__dirname + "/views/sample.06restAPI.order.pizza365.v3.4.html"))
})
//kết nối với mongooseDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365", function(error){
    if(error)throw error;
    console.log(`MongoDB successfully connected`);
})

//app sử dụng routes
app.use("/", drinkRouter);
app.use("/", voucherRouter);
app.use("/", userRouter);
app.use("/", orderRouter);
//listen when port is ready
app.listen(port, () =>{
    console.log(`App listening to port`, port);
})