
import { Container } from "@mui/system";
import "bootstrap/dist/css/bootstrap.min.css";
import LinkBar from "./components/LinkBarComponent/NavBar";
import CarouselAndHeader from "./components/CarouselAndHeader/CarouselAndHeader";
import WhyPizza365 from "./components/WhyPizza365/WhyPizza365";
import ComboMenu from "./components/ComboMenu/ComboMenu";
import PizzaType from "./components/PizzaType/PizzaType";
import Drink from "./components/Drink/Drink";
import OrderForm from "./components/OrderForm/OrderForm";
import Footer from "./components/Footer/Footer";
import { useState } from "react";
import OrderWindow from "./components/orderWindow/orderWindow";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "./action/order.action";
function App() {
  
  //Combo Size
    const [ComboSize, setComboSize] = useState(); 
  //Pizza type
    const [pizzaType, setPizzaType] = useState();
  //Drink
    const [chooseDrink, setOptionDrink] = useState(); // state when a drink is selected 
  //Form order
    const [fullName, setFullName] = useState();
    const [Email, setEmail] = useState();
    const [phoneNumber, setPhoneNumber] = useState();
    const [address, setAddress] = useState();
    const [voucher, setVoucher] = useState();
    const [message, setMessage] = useState();
  //hàm xử lý khi nút Gửi được ấn 
   const sendData = () => {
      console.log("Hello")
    };
  const dispatch = useDispatch();
  const style = {
      position: 'absolute' ,
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      width: 400,
      bgcolor: 'background.paper',
      border: '2px solid #000',
      boxShadow: 24,
      p: 4,
    };
    const openModal = useSelector(state => state.orderReducer.modal);
    const orderId = useSelector(state => state.orderReducer.orderID);
    const  handleClose = () =>{
      dispatch(closeModal());
    }
  return(
    <div className="App">
      <Modal
        open={openModal}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Mã order của bạn: {orderId}
          </Typography>
        </Box>
      </Modal>
      <form>
        <Container maxWidth="lg">
          {/* App bar  */}
          <LinkBar/>
          <CarouselAndHeader/>
            {/* Why pizza 365 */}
            <WhyPizza365/>
            {/* Combo */}
            <ComboMenu 
              //pros State
              prosComboSize = {ComboSize}
              // pros setSate
              prosSetComboSize = {setComboSize}/>
            {/* Pizza type */}
            <PizzaType 
              //pros State
              prosPizzaType = {pizzaType}
              // pros setSate 
              prosSetPizzaType = {setPizzaType}/>
            {/* Drink */}
            <Drink 
              //pros State
              prosDrink= {chooseDrink} 
              // pros setSate
              prosSetDrink = {setOptionDrink}/>
            {/* Form */}
            <OrderForm 
              //pros State
              prosComboSize = {ComboSize}
              prosPizzaType = {pizzaType}
              prosDrink= {chooseDrink}
              prosFullName = {fullName}
              prosEmail = {Email}
              prosPhoneNumber = {phoneNumber}
              prosAddress = {address}
              prosVoucher = {voucher}
              prosMessage = {message}
              // pros setSate
              prosSetFullName = {setFullName}
              prosSetEmail = {setEmail}
              prosSetPhoneNumber = {setPhoneNumber}
              prosSetAddress = {setAddress}
              prosSetVoucher = {setVoucher}
              prosSetMessage = {setMessage}
              proSendData = {sendData}
            />
            <OrderWindow/>
        </Container>
        {/* Footer */}
        <Footer /> 
      </form>
    </div>
  );
}

export default App;
