import { AppBar, Toolbar, Box, Button } from "@mui/material"
import * as Scroll from 'react-scroll';
function LinkBar (){
  var Link = Scroll.Link;
    return(
        <AppBar position="fixed" sx={{height: "50px",backgroundColor: "orange"}}>
            <Toolbar   sx={{ minHeight: "50px !important",
             mx:{
              md:"15px",
              xs:"0px"
            }}}>
              <Box sx={{height: "50px !important" , flexGrow: 1, display:{ xs: "flex", justifyContent:"space-between"}}} >
                <Button href="/" sx={{ my: 2, color: "black", fontSize:"13px"}}>
                    Trang Chủ
                </Button>
                <Button  sx={{ my: 2, color: "black", fontSize:"13px"}}>
                  <Link activeClass="active" smooth spy to="Combo">
                    Combo
                  </Link>
                </Button>
                <Button  sx={{ my: 2, color: "black", fontSize:"13px"}}>
                  <Link activeClass="active" smooth spy to="Pizza">
                    Loại Pizza
                  </Link>
                </Button>
                <Button  sx={{ my: 2, color: "black", fontSize:"13px"}}>
                  <Link activeClass="active" smooth spy to="Order">
                    Order
                  </Link>
                </Button>
              </Box>
            </Toolbar>
        </AppBar>
    )
}
export default LinkBar